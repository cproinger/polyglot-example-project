docker run --rm -v "$(pwd):/usr/src/app" \
     cproinger/centos7-python3-pyinstaller  "\
     set -x && \
     cd /usr/src/app && \
     python3.7 -m pip install --upgrade pip && \
     python3.7 -m pip install PyInstaller && \
     echo 'pip install spacy' && \
     python3.7 -m pip install spaCy && \
     python3.7 -m spacy download en && \
     python3.7 /usr/src/app/src/hellospacy.py -m en_core_web_sm:/usr/src/app/env/lib/python3.7/site-packages/en_core_web_sm/en_core_web_sm-2.1.0  -m 'x1:x2' &&\
     echo 'run pyinstaller' && \
     ./run-pyinstaller.sh &&\
     ./dist/hellospacy/hellospacy -m 'en_core_web_sm:/usr/src/app/env/lib/python3.7/site-packages/en_core_web_sm/en_core_web_sm-2.1.0' -m 'x1:x2' \
     "
