docker run --rm -v "$(pwd):/usr/src/app" \
    -v "$(pwd)/env/lib/python3.7/site-packages/en_core_web_sm/en_core_web_sm-2.1.0:/usr/src/app/env/lib/python3.7/site-packages/en_core_web_sm/en_core_web_sm-2.1.0" \
    centos:7.6.1810 /bin/bash -c "\
    /usr/src/app/dist/hellospacy/hellospacy -m 'en_core_web_sm:/usr/src/app/env/lib/python3.7/site-packages/en_core_web_sm/en_core_web_sm-2.1.0' \
    "