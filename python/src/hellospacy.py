import spacy
# import en_core_web_sm
import sys, getopt
from spacy.util import set_data_path

set_data_path('/usr/src/app/data')

en_core_web_sm_load_param = 'en_core_web_sm';
modelMap = {
    'en_core_web_sm' : 'en_core_web_sm',

}

if __name__ == "__main__":
    try:
        opts, args = getopt.getopt(sys.argv[1:],"m:")
        print('opts: ' + str(opts))
        print('args: ' + str(args))
        for opt, arg in opts:
            if opt == '-m':
                arg_arr = arg.split(':')
                modelMap[arg_arr[0]] = arg_arr[1]
    except getopt.GetoptError:
        print('hellospacy.py -m <name>:<path_to_model>')
        sys.exit(2)

# this loads the model as a python module which does not need symlinks
print('using modelMap: ' + str(modelMap))
print('usging path for en_core_web_sm-model: ', modelMap['en_core_web_sm'])
nlp = spacy.load(modelMap['en_core_web_sm'])
doc = nlp('Hello     World!')
for token in doc:
    print('"' + token.text + '"' + ': ' + token.pos_)

# "Hello"
# "    "
# "World"
# "!"